## Trabajo Práctico Final de Metodología de la Programación 2023 - Django -

Este proyecto se basa en una aplicacion para la gestion de una biblioteca mediante el framework Django.

## Equipo

Equipo.txt

## Autores

- Escudero Cuello, Juan Ignacio
- Mamani Lopez, Camila Sofía 
- Toconas, Daniel Maximiliano

## License

For open source projects, say now it is licenced.

## Project Status

If you have run out of energy or time for your project, put a note at the top of the README saying
that development has slowed down or stopped completely. Someone may choose to fork your project or 
volunteer to step in as a mantainer or orwner, allowing your project to keep going. You can also make
an explicit request for maintainers.


.